# xonstats-docker

Dockerize all the things!! Or more specifically, [XonStat](https://github.com/antzucaro/XonStat/) and [xonstatdb](https://github.com/antzucaro/xonstatdb).

## Requirements

Docker, follow the [docker installation instructions](https://docs.docker.com/engine/installation/) if you don't currently have it installed.

For development mode, you'll need xonstat and xonstatdb cloned to your host, `setup.sh` does that for you.

```
sh setup.sh
```

### (Optional) If you don't have docker-compose installed and would like to run it in inside a venv:

```
virtualenv -p /usr/bin/python3 venv
ln -s venv/bin/activate
source activate
pip install -r requirements-dev.txt
```

## Running

There are two modes, development and test. **Test is self-contained**, xonstat and xonstatdb git repositories are cloned inside the docker container with the intention of destroying the containers when you're done. **Development mounts xonstat and sonstatdb as volumes from the host** and persists if containers are destroyed.

To run all the things in **test mode**:

```
docker-compose -f docker-compose.test.yml up
```

To run all the things in **development mode**:

```
docker-compose up
```

Docker-compose will build your docker images on first run. 
